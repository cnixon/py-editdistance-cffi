from cffi import FFI
ffibuilder = FFI()




ffibuilder.set_source("_editdistance",
    """ // passed to the real C compiler
        #include "editdistance.h"
    """,
    libraries=["dl", "pthread", "gcc_s", "c", "m", "rt", "util" ],
    include_dirs=["ext/editdistance/include"],
    extra_objects=["ext/editdistance/target/release/libeditdistance.a"]
    )

ffibuilder.cdef(
        """
        uint32_t editdistance_checked(char const* root, char const* query);
        uint32_t editdistance_unchecked(char const* root, char const* query);
        """
        )

if __name__ == "__main__":
    ffibuilder.compile(verbose=True)

