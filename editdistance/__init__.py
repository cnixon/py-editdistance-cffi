import _editdistance

def editdistance(source: bytes, query: bytes):
    if isinstance(source, str):
        source = source.encode('utf-8')
    if isinstance(query, str):
        query = query.encode('utf-8')
    return _editdistance.lib.editdistance_checked(source, query)
