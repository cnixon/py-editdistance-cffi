from setuptools import setup

from rust_ext import build_rust_cmdclass, install_lib_including_rust

from setuptools.command import build_ext

# Ensure build_rust is run before build_ext
old_run = build_ext.build_ext.run
def new_run(self):
    # Make sure we run build_rust first
    self.run_command('build_rust')
    old_run(self)
build_ext.build_ext.run = new_run

setup(name='editdistance-cffi',
      version='0.1',
      description='CFFI bindings for rust implementation of Levenshtein distance',
      url='http://gitlab.com/cnixon/editdistance-cffi',
      author='Chris Nixon',
      author_email='chris.nixon@sigma.me.uk',
      license='WTFPL',
      packages=['editdistance'],
      install_requires=["cffi>=1.0.0"],
      setup_requires=["cffi>=1.0.0", "rust-ext>=0.1"],
      cmdclass={
          'build_rust': build_rust_cmdclass('ext/editdistance/Cargo.toml'),
          'install_lib': install_lib_including_rust
      },
      cffi_modules=["build_ext.py:ffibuilder"],
      zip_safe=False,
      )
