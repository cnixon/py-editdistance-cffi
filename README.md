System Dependencies:

* C build toolchain
* libffi
* cargo/rustc

Installation:

```
git clone --recursive https://gitlab.com/cnixon/py-editdistance-cffi.git
cd py-editdistance-cffi
pip install -r requirements.txt
python setup.py install
```
